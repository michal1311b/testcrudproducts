@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <edit-product :product="{{ $product }}"></edit-product>
        </div>
    </div>
</div>
@endsection

@section('content')
@endsection