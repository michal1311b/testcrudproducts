@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <product-list :products="{{ $products }}"></product-list>
        </div>
    </div>
</div>
@endsection

@section('content')
@endsection