<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Price;

class ProductController extends Controller
{
    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        \Log::info($input);

        $this->validate($request, [
            'name' => 'required|max:190',
            'description' => 'required',
        ]);

        $product = Product::create([
            'name' => $input['name'],
            'description' => $input['description']
        ]);

        foreach($input['prices'] as $price)
        {
            $price = Price::create([
                'product_id' => $product->id,
                'amount' => $price['amount']
            ]);
        }

        return response()->json([
            'message' => 'Data stored successfully'
        ], 201);  
    }

    public function index() {
        $products = Product::with('prices')->get();

        return view('products.index', compact('products'));
    }

    public function delete(Request $request) {
        $input = $request->all();

        $product = Product::with('prices')->find($input['id']);
        \Log::info($product);

        $product->delete();
        foreach($product->prices as $price)
        {
            $price->delete();
        }

        return response()->json([
            'message' => 'Data deleted successfully'
        ], 201);  
    }

    public function edit($id) {
        $product = Product::with('prices')->find($id);
        
        return view('products.edit', compact('product'));
    }

    public function deletePrice(Request $request) {
        $input = $request->all();
        
        $price = Price::find($input['id']);
        $price->delete();

        return response()->json([
            'message' => 'Price deleted successfully'
        ], 201);  
    }

    public function update(Request $request) {
        $input = $request->all();
        \Log::info($input);

        $product = Product::with('prices')->find($input['id']);
        \Log::info($product);
        $product->update([
            'name' => $input['name'],
            'description' => $input['description']
        ]);

        if(count($input['prices']) > 0)
        {
            foreach ($input['prices'] as $price) {
                $price = Price::create([
                    'product_id' => $product->id,
                    'amount' => $price['amount']
                ]);
            }
        }

        return response()->json([
            'message' => 'Product updated successfully'
        ], 201);  
    }
}
