<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/store-product', 'ProductController@store')->name('store-product');
Route::post('/delete-product', 'ProductController@delete')->name('delete-product');
Route::post('/delete-price/{product}', 'ProductController@deletePrice')->name('delete-price');
Route::post('/update-product/{product}', 'ProductController@update')->name('update-product');